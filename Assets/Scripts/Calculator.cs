﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculator
{
    public float Add(float a, float b)
    {
        return a + b;
    }

    public float Sub(float a, float b)
    {
        return a - b;
    }

    public float Div(float a, float b)
    {
        if (b == 0f)
        {
            throw new ArgumentException("Can't divide by zero!");
        }

        return a / b;
    }

    public float Mul(float a, float b)
    {
        return a * b;
    }

}
